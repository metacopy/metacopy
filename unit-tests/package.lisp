(in-package #:common-lisp-user)

(defpackage #:metacopy-test
  (:use #:common-lisp #:moptilities #:metacopy #:lift))